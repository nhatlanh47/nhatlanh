﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Account
{
    public int AccountId { get; set; }

    public int? CustomerId { get; set; }

    public string AccountName { get; set; } = null!;

    public string AccountType { get; set; } = null!;

    public decimal Balance { get; set; }

    public virtual Customer? Customer { get; set; }
}
